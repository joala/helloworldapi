package com.questulary.helloworldapi.controllers;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Test;

public class HelloWorldControllerTest {

//	class Task implements Runnable {
//		
//		String name;
//		Thread dependency;
//		
//		public Task(String name, Thread dependency) {
//			this.name = name;
//			this.dependency = dependency;
//		}
//
//		@Override
//		public void run() {
//			if (dependency != null) {
//				try {
//					dependency.join();
//				} catch (InterruptedException e) {
//					e.printStackTrace();
//				}
//			}
//			for (int i = 1; i <= 50; i++) {
//				if (i % 10 == 0) System.out.println(name); 
//				System.out.print(i + " ");
//			}
//		}
//		
//	}
//	
//	@Test
//	public void test() {
//		
//		Task task1 = new Task("task1", null);
//		Thread thread1 = new Thread(task1);
//		Task task2 = new Task("task2", thread1);
//		Thread thread2 = new Thread(task2);
//		Task task3 = new Task("task3", thread2);
//		Thread thread3 = new Thread(task3);
//		
//		thread1.start();
//		thread2.start();
//		thread3.start();
//	}
//	
//	@Test
//	public void division() {
//		
//		int num = 9;
//		System.out.println(num / 2.0);
//	}
	
	@Test
	public void findKthLargest() {
		
		int[] nums = {2, 10, 132, 4578, 50, 67, 1002, 596, 500, 23, 71, 80, 80, 80, 66, 41};
		long begin = System.nanoTime();
		System.out.println(concatenationSum(nums));
		long end = System.nanoTime();
		long totalTime = end - begin;
		System.out.println(totalTime);
		begin = System.nanoTime();
		System.out.println(concatenationSum2(nums));
		end = System.nanoTime();
		long totalTime2 = end - begin;
		System.out.println(totalTime2);
		System.out.println((double)totalTime / totalTime2);
    }
	
	private long concatenationSum(int[] nums) {
		long result = 0l;
		List<String> concatenations = new ArrayList<>();
		for (int i = 0; i < nums.length; i++) {
			for (int j = 0; j < nums.length; j++) {
				concatenations.add(nums[i] + "" + nums[j]);
			}
		}
		for (String concatenation : concatenations) {
			result += Long.valueOf(concatenation);
		}
		return result;
	}
	
	private long concatenationSum2(int[] nums) {
		long result = 0l;
		List<Long> concatenations = new ArrayList<>();
		for (int i = 0; i < nums.length; i++) {
			for (int j = 0; j < nums.length; j++) {
				int power = (int) Math.log10(nums[j]);
				long concatenation = (nums[i] * (long)Math.pow(10, power + 1));
				concatenation += nums[j];
				concatenations.add(concatenation);
			}
		}
		for (Long concatenation : concatenations) {
			result += concatenation;
		}
		return result;
	}
	
	@Test
	public void twimstAndRotateMatix() {
		int[][] matrix = {
				{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,16}
		};
		printMatrix(matrix);
		System.out.println();
		matrix = rotateMatrix(matrix);
		printMatrix(matrix);
		System.out.println();
		rotateMatrix2(matrix);
		printMatrix(matrix);
		System.out.println();
		matrix = flipMainDiagonal(matrix);
		printMatrix(matrix);
		System.out.println();
		matrix = flipSecondaryDiagonal(matrix);
		printMatrix(matrix);
		System.out.println();
	}
	
	private int[][] rotateMatrix(int[][] matrix) {
		int[][] result = new int[matrix[0].length][matrix.length];
		for (int row = 0; row < matrix.length; row++) {
			int[] elements = matrix[row];
			for (int column = 0; column < matrix.length; column++) {
				result[column][matrix.length - row - 1] = elements[column];
			}
		}
		return result;
	}
	
	private void rotateMatrix2(int[][] matrix) {
        for (int layer = 0; layer < matrix.length / 2; layer++) {
            for (int element = layer; element < matrix.length - layer - 1; element++) {
                int temp = matrix[layer][element];
                matrix[layer][element] = matrix[element][matrix.length - 1 - layer];
                matrix[element][matrix.length - 1 - layer] = matrix[matrix.length - 1 - layer][matrix.length - 1 - element];
                matrix[matrix.length - 1 - layer][matrix.length - 1 - element] = matrix[matrix.length - 1 - element][layer];
                matrix[matrix.length - 1 - element][layer] = temp;
            }
        }
	}
	
	private int[][] flipMainDiagonal(int[][] matrix) {
		int[][] result = new int[matrix[0].length][matrix.length];
		for (int row = 0; row < matrix.length; row++) {
			for (int column = 0; column < matrix.length; column++) {
				result[column][row] = matrix[row][column];
			}
		}
		return result;
	}
	
	private int[][] flipSecondaryDiagonal(int[][] matrix) {
		int[][] result = new int[matrix[0].length][matrix.length];
		for (int row = 0; row < matrix.length; row++) {
			for (int column = 0; column < matrix.length; column++) {
				result[row][column] = matrix[matrix.length - column - 1][matrix.length - row - 1];
			}
		}
		return result;
	}

	private void printMatrix(int[][] matrix) {
		System.out.print('[');
		for (int row = 0; row < matrix.length; row++) {
			if (row == matrix.length - 1) {
				System.out.print(Arrays.toString(matrix[row]));
			} else {
				System.out.printf("%s,%n", Arrays.toString(matrix[row]));
			}
		}
		System.out.println(']');
	}
}
