package com.questulary.helloworldapi.controllers;

import static java.lang.String.format;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping(value="/helloworld")
public class HelloWorldController {

	@RequestMapping(value = "/{name}", method = GET)
	public @ResponseBody String sayHelloWorld(@PathVariable String name) {
		
		return format("Hello, world! My name is %s.", name);
	}
}
